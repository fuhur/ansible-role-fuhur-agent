# fuhur-agent

Installs and configures Fuhur Agent service.

## Requirements

The role works together with ansible-role-fuhur-api


## Role Variables

### Mandantory variables

```yaml
fuhur_sign_secret: "Change me"
fuhur_jobs:
  example:
    - command: "example.sh"
      dir: "example"
      job: "example"
      env: ["EXAMPLE=FOOBARBAZ"]
    - command: "example2.sh"
      dir: "example"
      job: "example"
      env: ["EXAMPLE2=FOOBARBAZ"]
```

#### fuhur_sign_secret

That must be a random string which is shared with the API. API will sign the jobs with that secret and agents will check if the signature is valid.

#### fuhur_jobs

List of jobs what agent must run.

### Other variables

```yaml
fuhur_agent_version: "v2.0.5"
fuhur_agent_download_url: "https://gitlab.com/fuhur/agent/-/jobs/artifacts/{{fuhur_agent_version}}/download?job=fuhur_agent"

fuhur_agent_service_name: "fuhur-agent"
fuhur_agent_systemd_service_file_path: "/etc/systemd/system/{{ fuhur_agent_service_name }}.service"
fuhur_agent_config_file_path: "/etc/fuhur.json"
fuhur_agent_config_dir_path: "/etc/fuhur.d"
fuhur_agent_scripts_dir_path: "/usr/local/share/fuhur"
```

## Example Playbook

```yaml
- hosts: servers
  vars:
  fuhur_sign_secret: "Change me"
  fuhur_jobs:
    example:
      - command: "example.sh"
        dir: "example"
        job: "example"
        env: ["EXAMPLE=FOOBARBAZ"]
      - command: "example2.sh"
        dir: "example"
        job: "example"
        env: ["EXAMPLE2=FOOBARBAZ"]
  roles:
    - { role: fuhur.agent }
```

## License

Apache License 2.0
